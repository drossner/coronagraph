import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class CoronaGraph extends Application {
    private HttpResponse<String> covid19;
    private MenuItem changeCountries;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Corona Cases");

        ArrayList<String> countries = new ArrayList<>();
        countries.add("Germany");
        countries.add("Italy");
        IllnessStatus status = IllnessStatus.confirmed;

        primaryStage.setScene(getSceneFor(countries, status));
        primaryStage.show();
    }

    public Scene getSceneFor(List<String> countries, IllnessStatus status) {
        //axis
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("days since cases > 50");
        yAxis.setLabel(status.toString() + " cases");
        xAxis.setTickUnit(2);
        yAxis.setTickUnit(2);

        this.changeCountries = new MenuItem("Change countries");
        Menu menu = new Menu("Settings");
        menu.getItems().add(this.changeCountries);
        MenuBar menuBar = new MenuBar(menu);

        final LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
        chart.setTitle("Corona virus cases");

        for (String country : countries) {
            chart.getData().add(getChartSeriesFor(country, status));
        }

        BorderPane pane = new BorderPane(chart, menuBar, null, null, null);

        return new Scene(pane, 800, 600);
    }

    private XYChart.Series getChartSeriesFor(String nation, IllnessStatus status) {
        Country country = new Country(nation);
        try {
            requestFor(country);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        XYChart.Series series = new XYChart.Series();
        switch (status) {
            case confirmed:
                series = getSeriesFor(country.getConfPersons());
                break;
            case dead:
                series = getSeriesFor(country.getDeadPersons());
                break;
            case recovered:
                series = getSeriesFor(country.getRecovPersons());
                break;
        }
        series.setName(country.getName());

        return series;
    }

    private XYChart.Series getSeriesFor(List<Integer> cases) {
        XYChart.Series series = new XYChart.Series();
        int index = 0;
        for (int c : cases) {
            series.getData().add(new XYChart.Data(index++, c));
        }
        return series;
    }


    private void requestFor(Country country) throws IOException, InterruptedException {
        HttpResponse<String> response = getStringHttpResponseCovid19();
        JSONArray json = new JSONObject(response.body()).getJSONArray(country.getName());
        Iterator<Object> iterator = json.iterator();
        iterator.forEachRemaining(e -> {
            int confirmed = ((JSONObject) e).getInt("confirmed");
            int deaths = ((JSONObject) e).getInt("deaths");
            int recovered = ((JSONObject) e).getInt("recovered");
            if (confirmed > 50) {
                country.getConfPersons().add(confirmed);
                country.getDeadPersons().add(deaths);
                country.getRecovPersons().add(recovered);
            }
        });
    }

    private HttpResponse<String> getStringHttpResponseCovid19() throws IOException, InterruptedException {
        if (this.covid19 == null) {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://pomber.github.io/covid19/timeseries.json"))
                    .GET()
                    .build();
            this.covid19 = client.send(request, HttpResponse.BodyHandlers.ofString());
        }
        return this.covid19;
    }

    public List<String> getAllCountries() {
        HttpResponse<String> response = null;
        try {
            response = getStringHttpResponseCovid19();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        assert response != null;
        JSONObject json = new JSONObject(response.body());
        Set keySet = json.keySet();
        ArrayList<String> keys = new ArrayList<>(keySet);
        ArrayList<String> result = new ArrayList<>();
        for (String key : keys) {
            JSONArray array = json.getJSONArray(key);
            JSONObject last = array.getJSONObject(array.length() - 1);
            if (last.getInt("confirmed") >= 50) {
                result.add(key);
            }
        }
        result.sort(null);
        return result;
    }

    public MenuItem getChangeCountries() {
        return changeCountries;
    }
}
